# AS3 Deobfuscator

Tool for deobfuscating AS3 SWF files.

## Requirements

The following dependencies are required:
- `python3`
- `rabcdasm` (bundled inside):
    - `rabcdasm/src`: git submodule pointing to the [RABCDAsm repository](https://github.com/CyberShadow/RABCDAsm)
    - `rabcdasm/linux`: compiled Linux (64 bits) binaries
    - `rabcdasm/windows`: compiled Windows (64 bits) binaries

## Usage

```sh
$ python3 scripts/deobfu.py <file.swf> <mappings.txt>
```
Will replace all identifiers in `file.swf` according to the `mappings.txt`, and write the resulting file to `file-deobfu.swf`.

### `mappings.txt` format

The mappings file lists the deobfuscated names for each obfuscated identifier, with one pair per line.
Here's an example:

```
# Whole-line comment
C5;F\x01        example
V\x05\x0e{\x01  foo     # End-of-line comment
(/bv\x20g       bar
gPb\n5		baz
```

Comments are allowed following these requirements:
- there **must** be a space **after** the `#`.
- if the comment is an end-of-line comment, there **must** be a space **before** the `#`.

**Notes:**
- Non-printable characters are escaped.
- Spaces (` `) must also be escaped; for example, `(/bv g` becomes `(/bv\x20g`.
- When copying identifiers from JPEXS, make sure to remove the enclosing `§§`.

## License

This project is licensed under the GPLv3 License.
